﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadCurrentScene : MonoBehaviour
{
    [NonSerialized]
    public bool ClearedScreen = false;
    [NonSerialized]
    public bool WinScreen = false;

    public void curLoadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadNextLevel()
    {
        //Set ClearedScreen to false before loading next scene
        //since we will no longer be at the cleared screen
        ClearedScreen = false;
        WinScreen = false;
        if (MySceneManager.CurrentLevel < (SceneManager.sceneCountInBuildSettings - 1))
        {
            MySceneManager.CurrentLevel = MySceneManager.CurrentLevel + 1;
            SceneManager.LoadScene(MySceneManager.CurrentLevel);
        }
        else
        {
            LoadWinScreen();
        }
    }

    public void LoadClearedScreen()
    {
        //We are at ClearedScreen so set ClearedScreen to true
        ClearedScreen = true;
        WinScreen = false;
        SceneManager.LoadScene(SceneManager.sceneCountInBuildSettings - 1);
    }

    public void LoadWinScreen()
    {
        //we are at win screen so set WinScreen to true
        WinScreen = true;
        ClearedScreen = false;
        SceneManager.LoadScene(SceneManager.sceneCountInBuildSettings - 2);
    }

    public void RestartGame()
    {
        //restarting game so ClearedScreen and WinScreen should be false
        ClearedScreen = false;
        WinScreen = false;
        SceneManager.LoadScene(1);
        MySceneManager.CurrentLevel = 0;
    }

}
