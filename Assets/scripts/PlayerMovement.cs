﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Initialize initer;
    private Rigidbody2D rb;
    public Animator animator;
    public Weapon weaponScript;
    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;

    private void Start()
    {
        rb = FindObjectOfType<Player>().gameObject.GetComponent<Rigidbody2D>();
    }

// Update is called once per frame
void Update()
    {
            //set horizontal move velocity equal to the rigidbody's velocity in x
            horizontalMove = rb.velocity.x;
            //set the speed of the animator
            animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

            //Player Input
            if (Input.GetButtonDown("Jump"))
            {
                //animate accordingly
                animator.SetBool("IsJumping", jump);
            }

            if (Input.GetButtonUp("Jump"))
            {
                //animate accordingly
                animator.SetBool("IsJumping", jump);
            }

            if (Input.GetButtonDown("Crouch"))
            {
                //we are crouching
                crouch = true;

                //animate accordingly
                animator.SetBool("IsCrouching", crouch);
            }

            if (Input.GetButtonUp("Crouch"))
            {
                //no longer crouching
                crouch = false;

                //animate accordingly
                animator.SetBool("IsCrouching", crouch);
            }

            if (Input.GetButtonDown("Fire1"))
            {
                //animate attacking
                animator.SetBool("IsAttacking", true);
            }

            if (Input.GetButtonUp("Fire1"))
            {
                //stop animating attack
                animator.SetBool("IsAttacking", false);
            }

            if (Input.GetButtonDown("Start"))
            {
            //reload the scene
            initer.ReloadScene();


            }

    }

    public void OnCrouching(bool IsCrouching)
    {
        //handle crouching stuff
        animator.SetBool("IsCrouching", IsCrouching);
    }
    private void FixedUpdate()
    {

        jump = false;

        //if there is a hole in the ground then teleport player to the top
        if (gameObject.transform.position.y < -36)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, 5, gameObject.transform.position.z);
        }
    }
}
