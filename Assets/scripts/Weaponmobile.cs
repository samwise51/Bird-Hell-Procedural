﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weaponmobile : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    
    public void Shoot ()
    {
        //shooting logic
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);

    }
}
