﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopulateGrid : MonoBehaviour
{
    public GameObject prefabButton;
    public int numberToCreate; //how many objects to create

    [SerializeField]
    private Sprite[] iconSprites;
    [SerializeField]
    private GameObject[] prefabSprite;

    public GameObject content;


    // Start is called before the first frame update
    void Start()
    {
        Populate();

    }

    void Populate()
    {
        GameObject newObj; //create game object instance

        for (int i = 0; i < (numberToCreate); i++)
        {
            newObj = Instantiate(prefabButton, transform); //create buttons
            newObj.GetComponent<InventoryButton>().SetIcon(iconSprites[i]);  //set icons 
            newObj.name = i.ToString();
        }

        UnityEngine.UI.Button[] btn = gameObject.GetComponentsInChildren<UnityEngine.UI.Button>();
        //for (int i = 0; i < (numberToCreate); i++)
        foreach (Button i in btn)
        {
            i.onClick.AddListener(delegate { BtnClick(int.Parse(i.name)); });

        }
    }

    public void BtnClick(int num)
    {
        GameObject newobj;

        newobj = Instantiate(prefabSprite[num], new Vector3 (0,0,0), Quaternion.identity) as GameObject;
        newobj.SetActive(true);
    }

    public class PlayerItem
    {
        public Sprite iconSprite;

    }

}
