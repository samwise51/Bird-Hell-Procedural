﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int health = 100;
    public GameObject deathEffect;
    public int PlayerDamage = 10;
    public deathcheck death;
    private Initialize initer;

    private float dTime;
    private float tTime;
    private bool damaged = false;
    Color red = new Color(255, 0, 0);
    Color white = new Color(255, 255, 255);

    private void Update()
    {
        dTime += Time.deltaTime;
        DamageEffect();
    }

    private void Start()
    {
        initer = FindObjectOfType<Initialize>();
        death = initer.gameObject.GetComponent<deathcheck>();
    }

    private void FixedUpdate()
    {
        tTime += Time.fixedDeltaTime;
        
    }

    public void TakeDamage( int damage)
    {
        initer.enemyHitSound.Play();
        health -= damage;
        damaged = true;
        if (health <= 0)
        {
            Die();
        }
    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>().gameObject.name == "player")
        {
            Player player = collision.gameObject.GetComponent<Player>();
            if (player != null)
            {
                if (tTime > 1)
                {
                    player.TakeDamage(PlayerDamage);
                    tTime = 0;
                }
            }

        }
    }

    void DamageEffect()
    {
        if (damaged == true)
        {
            GetComponent<SpriteRenderer>().color = red;
        }
        if (dTime >= 0.5)
        {
            GetComponent<SpriteRenderer>().color = white;
            damaged = false;
            dTime = 0;
        }
    }

    void Die()
    {
        initer.enemyDeathSound.Play();
        //kill bird
        death.KilledEnemy(gameObject);
        if(death.AreOpponentsDead() == true)
        {
            UnityEngine.Debug.Log("All Birds Dead");
        }
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        //Destroy the Object
        //gameObject.SetActive(false);
        Destroy(gameObject);
    }

    /*public static bool IsNullOrEmpty(GameObject[] array) 
    {
        if (array == null || array.Length == 0)
            return true;
        else
            return array.All(item => item == null);
    }*/

}
