﻿using UnityEngine;

public class ButtonEvents : MonoBehaviour
{   
    //declare some stuff
    public Pause PauseScript;
    public LoadCurrentScene LoadSceneScript;

    //define enum for buttons
    public enum WhichButtonFocus
    {
        Pause,
        Cleared,
        Restart
    }

    private void Update()
    {
        // Set default focus state of buttons
        WhichButtonFocus myVar = WhichButtonFocus.Pause;

        if (Input.GetButtonDown("Cancel"))
        {

            if (PauseScript.isPaused == true)
            {
                myVar = WhichButtonFocus.Pause;
            }
            if (PauseScript.isPaused == false)
            {
                myVar = WhichButtonFocus.Pause;
            }
            if (LoadSceneScript.ClearedScreen == true)
            {
                myVar = WhichButtonFocus.Cleared;
            }
            if (LoadSceneScript.WinScreen == true)
            {
                myVar = WhichButtonFocus.Restart;
            }

            switch (myVar)
            {
                case WhichButtonFocus.Pause:
                    if (PauseScript.isPaused == true)
                    {
                        PauseScript.ContinueGame();
                    } 
                    else 
                    {
                        PauseScript.PauseGame();    
                    }                    
                    break;
                case WhichButtonFocus.Cleared:
                    LoadSceneScript.LoadNextLevel();
                    break;
                case WhichButtonFocus.Restart:
                    LoadSceneScript.curLoadScene();
                    break;
            }
        }
    }
}
