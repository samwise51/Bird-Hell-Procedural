﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayHealth : MonoBehaviour
{
    public GameObject player;
    public Text textbox;

    void Start()
    {
        textbox = GetComponent<Text>();
    }

    void FixedUpdate()
    {
        textbox.text = "Health " + player.GetComponent<Player>().health;
    }
}
