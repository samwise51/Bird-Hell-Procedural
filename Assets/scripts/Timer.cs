﻿using UnityEngine;

public class Timer : MonoBehaviour
{
    //define some stuff
    public float milliseconds;

     // Update is called once per frame
    void Update()
    {
        milliseconds = (int)(Time.realtimeSinceStartup * 1000f);
    }

    public static Timer instance;

    void Awake()
    {
        if (instance != null)
        {
            if (instance != this)
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }

    public float GetTimeSinceStart()
    {
        return milliseconds;
    }

    public float GetTimeDiff(float time1, float time2)
    {
        float myTimeDiff = time2 - time1;
        return myTimeDiff;
    }
}
