﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private RectTransform ScrollMenu;
    
    public Button btnOpen, btnClose;

    // Start is called before the first frame update
    void Start()
    {
        btnOpen.onClick.AddListener(delegate { OpenScrollMenu(); });
        btnClose.onClick.AddListener(delegate { CloseScrollMenu(); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenScrollMenu()
    {
        ScrollMenu.DOAnchorPos(new Vector2(600, 168), 0.25f);
        btnClose.gameObject.SetActive(true);
        btnOpen.gameObject.SetActive(false);
    }

    public void CloseScrollMenu()
    {
        ScrollMenu.DOAnchorPos(new Vector2(1371, 168), 0.25f);
        btnOpen.gameObject.SetActive(true);
        btnClose.gameObject.SetActive(false);

    }
    
}
