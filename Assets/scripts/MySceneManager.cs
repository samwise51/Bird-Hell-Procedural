﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MySceneManager
{
    private static int currentLevel = 0;

    public static int CurrentLevel
    {
        get
        {
            return currentLevel;
        }
        set
        {
            currentLevel = value;
        }
    }
}
