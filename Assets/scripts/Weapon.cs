﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    [SerializeField]
    private LayerMask whatIsEnemy;
    [SerializeField]
    private float attackradius = 1f;
    private Collider2D cPoints;
    private Enemy emy;

    // Update is called once per frame
    void Update()
    {
       if (Input.GetButtonDown("Fire1"))
        {
            if (cPoints = Physics2D.OverlapCircle(firePoint.position, attackradius, whatIsEnemy))
            {
                //define enemy we just hit
                emy = cPoints.gameObject.GetComponent<Enemy>();
                //take full damage when within striking range of sword
                emy.TakeDamage(100);
            }
            else
            {
                Shoot();
            }
        } 
    }

    void Shoot ()
    {
        //shooting logic
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);

    }
}
