﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deathcheck : MonoBehaviour
{
    public List<GameObject> listOfEnemies = new List<GameObject>();

    private void Start()
    {

    }

    public void KilledEnemy(GameObject enemy)
    {
        listOfEnemies.Remove(enemy);
    }

    public bool AreOpponentsDead()
    {
        if (listOfEnemies.Count <= 0)
        {
            // They are dead!
            return true;
        }
        else
        {
            // They're still alive dangit
            return false;
        }
    }
}
