﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Player : MonoBehaviour
{
    private Initialize initer;
    public int health = 100;
    public GameObject deathEffect;
    private float tTime;
    public bool damaged = false;
    public bool playerdeath = false;
    Color red = new Color(255, 0, 0);
    Color white = new Color(255, 255, 255);

    private void Start()
    {
        initer = FindObjectOfType<Initialize>();
    }
    void FixedUpdate()
    {
        tTime += Time.deltaTime;
        DamageEffect();
    }

    public void TakeDamage(int damage)
    {
        initer.playerHitSound.Play();
        health -= damage;
        damaged = true;
        if (health <= 0)
        {
            playerdeath = true;
            Die();
        }
    }

    void DamageEffect()
    {
        if (damaged == true)
        {
            GetComponent<SpriteRenderer>().color = red;
        }
        if (tTime >= 0.5)
        {
            GetComponent<SpriteRenderer>().color = white;
            damaged = false;
            tTime = 0;
        }
    }

    void Die()
    {
        initer.playerDeathSound.Play();
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        gameObject.SetActive(false);
        //Destroy(gameObject);
    }
}
