﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drag : MonoBehaviour
{
    //Draggable Objects
     float distance = 10;
     void OnMouseDrag()
     {
         Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
         Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
         transform.position = new Vector3(Mathf.Round(objPosition.x),Mathf.Round(objPosition.y),distance);

     }

    

}
