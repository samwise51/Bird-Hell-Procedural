﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    [SerializeField] private GameObject pausePanel;
    public bool isPaused;
    public Button btnPause;
    //Mobile Only
    //public Button btnJump;
    //public Button btnFire;
    //public FixedJoystick Joystick;
    public Button btnRestart;
    public LoadCurrentScene LoadScene;

    void Start()
    {
        pausePanel.SetActive(false);
        btnPause.onClick.AddListener(delegate { PausePress(); });
        btnRestart.onClick.AddListener(delegate { RestartPress(); });
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (pausePanel.activeInHierarchy != true)
            {
                PauseGame();
            }
            else
            {
                ContinueGame();
            }
        }

    }

    public void RestartPress()
    {
        ContinueGame();
        LoadScene.curLoadScene();
    }

    public void PausePress()
    {
        if (isPaused == true)
        {
            isPaused = false;
        }
        else
        {
           isPaused = true;
        }
        if (isPaused == true)
        {
            //Mobile Only
            //btnJump.gameObject.SetActive(false);
            //btnFire.gameObject.SetActive(false);
            //Joystick.gameObject.SetActive(false);
            btnRestart.gameObject.SetActive(true);
            PauseGame();
        }
        else
        {
            //Mobile Only
            //btnJump.gameObject.SetActive(true);
            //btnFire.gameObject.SetActive(true);
            //Joystick.gameObject.SetActive(true);
            btnRestart.gameObject.SetActive(false);
            ContinueGame();

        }
    }

    public void PauseGame()
    {
        isPaused = true;
        Time.timeScale = 0;
        pausePanel.SetActive(true);
        //Prevent other things from running while game is paused
        //Disable scripts that still work while timescale is set to 0
        //
    }
    public void ContinueGame()
    {
        isPaused = false;
        Time.timeScale = 1;
        pausePanel.SetActive(false);
        //enable the scripts again
        //
    }
}
