﻿    using UnityEngine;
using UnityEngine.SceneManagement;
using Pathfinding;

public class Initialize : MonoBehaviour
{
    public static Initialize instance;
    [SerializeField] private MapGenerator mGen;
    public AudioSource BackgroundMusic;
    public AudioSource playerHitSound;
    public AudioSource blastersound;
    public AudioSource playerDeathSound;
    public AudioSource enemyHitSound;
    public AudioSource enemyDeathSound;

    EdgeCollider2D[] thisCollider;
    Bounds bounds;
    public GameObject prefabEnemy;
    [SerializeField] private Transform plyr;
    public int enemyDesired = 100;
    int e = 0;
    private AIDestinationSetter aiDestSetter; 
    public NewCharacterController2D ctrl;
    public deathcheck death;

    // Start is called before the first frame update
    void Start()
    {

        //translate player position
        TranslatePlayer();

        //spawn enemies
        SpawnEnemies();

        //Play Background Music
        BackgroundMusic.Play();
    }

    void Awake()
    {
        if (instance != null)
        {
            if (instance != this)
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }

        //Generate Map
        mGen.GenerateMap();
    }

    public void TranslatePlayer()
    {
        thisCollider = mGen.GetComponents<EdgeCollider2D>();
        bounds = thisCollider[0].bounds;
        //plyr.SetPositionAndRotation(new Vector3(thisCollider[0].points[1].x + 10, thisCollider[0].points[1].y, 0), Quaternion.identity);
        plyr.SetPositionAndRotation(new Vector3(thisCollider[0].bounds.center.x - 5, thisCollider[0].bounds.center.y - 5, 0), Quaternion.identity);
    }

    public void SpawnEnemies()
    {
        while (e < enemyDesired)
        {
            float x = thisCollider[0].bounds.center.x;
            float y = thisCollider[0].bounds.center.y;
            Vector3 spawn = new Vector3(x, y, 0);
            GameObject spawned = Instantiate(prefabEnemy, spawn, Quaternion.identity);
            spawned.gameObject.GetComponent<AIDestinationSetter>().target = plyr;
            spawned.gameObject.GetComponent<AIPath>().maxSpeed = UnityEngine.Random.Range(2, 10);
            death.listOfEnemies.Add(spawned);
            e++;

        }
        e = 0;
    }

    public void ReloadScene()
    {
        //define enemy objects we want to destroy
        GameObject[] emy;
        emy = GameObject.FindGameObjectsWithTag("Enemy");
        //destroy them with vengence
        for (int i = 0; i <= (emy.Length - 1); i++)
        {
            Destroy(emy[i].gameObject, 0);
        }

        //Generate a new Map
        mGen.GenerateMap();
        
        //translate player position and make sure player is active and full health
        TranslatePlayer();
        ctrl.facingDirection = 1;
        plyr.gameObject.SetActive(true);
        plyr.GetComponent<Player>().health = 100;
        //Re-scan AiPath Graph
        AstarPath.active.Scan();

        //clean up enemy list count
        foreach (GameObject enemy in emy)
        {
            death.listOfEnemies.Remove(enemy.gameObject);
        }
        //spawn enemies
        SpawnEnemies();


    }
}
