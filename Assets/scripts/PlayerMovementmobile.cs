﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovementmobile : MonoBehaviour
{
    public CharacterController2D controller;
    public Animator animator;
    public FixedJoystick Joystick;
    public Button btnJump;
    public Button btnFire;
    public float runSpeed = 30f;
    public Weaponmobile weaponScript;
    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;

    private void Start()
    {
        //initialize jump button
        btnJump.onClick.AddListener(delegate { JumpPress(); });
        btnFire.onClick.AddListener(delegate { FirePress(); });
    }
    // Update is called once per frame
    void Update()
    {
        //set the speed of the player's movement
        horizontalMove = Joystick.Horizontal * runSpeed * Time.fixedDeltaTime;
 
        //set the speed of the animator
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (Input.GetButtonUp("Fire1"))
        {
            animator.SetBool("IsAttacking", false);
        }

    }
    public void JumpPress()
    {
        jump = true;
        animator.SetBool("IsJumping", true);
    }

    public void FirePress()
    {
        weaponScript.Shoot();
    }

    public void OnLanding()
    {
        jump = false;
        animator.SetBool("IsJumping", false);
    }

    public void OnCrouching(bool IsCrouching)
    {
        animator.SetBool("IsCrouching", IsCrouching);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.otherCollider.gameObject.name == "player")
        {
            animator.SetBool("IsCrouching", false);
        }
    }
    private void FixedUpdate()
    {


        //Move Character
        controller.Move(horizontalMove, crouch, jump);
        jump = false;

        //if the player is out of vertical bounds
        if(gameObject.transform.position.y < -5)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, 5,gameObject.transform.position.z);
        }
    }
}
