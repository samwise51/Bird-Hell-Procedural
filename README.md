# birdhell
Bird Hell

# Fix/Add this:
- pause menu issue using joystick
- vary path of enemies via behavior states (Idle, attacking, patrol, ...)
- fix ButtonEvents.cs (related to pause menu issue)
- add sounds for background music, hits, explosions, jumping/landing, bird yells, ...
- finish level editor (figure out how to add box colliders programatically for custom maps)
- adjust effect of artificial gravity applied in "Better Jump Code" comments
